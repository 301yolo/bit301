##Work Items List for Educational wiki for Java programing – if else


| No | Work Item | Est Hours | Assigned to | Hours Worked | Status |
|----|-----------|-----------|-------------|--------------|--------|
| 1	   |  Educational Wiki Project      		| 44 Day     | Hay Lik Sheng        | 8 hour per days         | Active   |
| 1.1  |  Wiki Page Content     				| 21 Day     | Hay Lik Sheng        | 8 hour per days         | Active   |
| 1.2  |  Wiki Page Video      					| 23 Day     | Hay Lik Sheng        | 8 hour per days         | Active   |
| 1.3  |  Wiki Page Image     					| 23 Day     | Hay Lik Sheng        | 8 hour per days         | Active   |
| 2.0  |  Initiating Tasks 						| 10 Day     | Tan Khoon Hoong      | 8 hour per days         | Active   |
| 2.1  |  Three Sphere Model     				|  4 Day     | Tan Khoon Hoong      | 8 hour per days         | Active   |
| 2.2  |  StakeHolder Analysis     				|  4 Day     | Tan Khoon Hoong      | 8 hour per days         | Active   |
| 2.3  |  Business Case     					|  6 Day     | Tan Khoon Hoong      | 8 hour per days         | Active   |
| 2.4  |  Project Charter     					|  5 Day     | Tan Khoon Hoong      | 8 hour per days         | Active   |
| 3.0  |  Planning Tasks        				| 5 Day      | Chong Kam Fai        | 8 hour per days         | Active   |
| 3.1  |  Scope Statement        				| 4 Day      | Chong Kam Fai        | 8 hour per days         | Active   |
| 3.2  |  Work Breakdown Structure        		| 4 Day      | Chong Kam Fai        | 8 hour per days         | Active   |
| 3.3  |  Work Items List        				| 4 Day      | Chong Kam Fai        | 8 hour per days         | Active   |
| 3.4  |  Baseline Gantt Chart        			| 5 Day      | Chong Kam Fai        | 8 hour per days         | Active   |
| 4.0  |  Planning Tasks 2    					| 17 Day     | Hay Lik Sheng        | 8 hour per days         | Active   |
| 4.1  |  Cost Estimate    						| 14 Day     | Hay Lik Sheng        | 8 hour per days         | Active   |
| 4.2  |  Quality Management Plan   			| 14 Day     | Hay Lik Sheng        | 8 hour per days         | Active   |
| 4.3  |  Risk Management Plan    				| 14 Day     | Hay Lik Sheng        | 8 hour per days         | Active   |
| 5.0  |  Executing Tasks       				| 6 Day      | Tan Khoon Hoong      | 8 hour per days         | Standby  |
| 6.0  |  Executing Tasks 2      		 		| 9 Day      | Chong Kam Fai        | 8 hour per days         | Standby  |
| 7.0  |  Monitoring and Controlling Task 1   	| 7 Day      | Hay Lik Sheng        | 8 hour per days         | Standby  |
| 7.1  |  Issue Tracking   						| 6 Day      | Hay Lik Sheng        | 8 hour per days         | Standby  |
| 7.2  |  Tracking Gantt Chart  				| 6 Day      | Hay Lik Sheng        | 8 hour per days         | Standby  |
| 7.3  |  Progress Report  						| 2 Day      | Hay Lik Sheng        | 8 hour per days         | Standby  |
| 8.0  |  Monitoring and Controlling Task 2 	| 12 Day     | Tan Khoon Hoong      | 8 hour per days         | Standby  |
| 8.1  |  Earned Value Analysis 				| 9 Day      | Tan Khoon Hoong      | 8 hour per days         | Standby  |
| 8.2  |  Issue Tracking 						| 9 Day      | Tan Khoon Hoong      | 8 hour per days         | Standby  |
| 8.3  |  Tracking Gantt Chart 					| 9 Day      | Tan Khoon Hoong      | 8 hour per days         | Standby  |
| 8.4  |  Quality Control 						| 9 Day      | Tan Khoon Hoong      | 8 hour per days         | Standby  |
| 8.5  |  Risk Management 						| 10 Day     | Tan Khoon Hoong      | 8 hour per days         | Standby  |
| 9.0  |  Closing Tasks      					| 5 Day      | Chong Kam Fai        | 8 hour per days         | Standby  |
| 9.1  |  Final Project Presentation      		| 5 Day      | Chong Kam Fai        | 8 hour per days         | Standby  |
| 9.2  |  Lesson Learned Report      			| 5 Day      | Chong Kam Fai        | 8 hour per days         | Standby  |