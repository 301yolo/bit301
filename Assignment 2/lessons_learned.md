#Lessons Learned Report

**Prepared by: Hay Lik Sheng**

**Date: 5/11/2015**

**Project Name: Java If Statement Wiki Project**
	
**Project Sponsor: Project Stakeholders**
	
**Project Manager: Tan Khoon Hoong**
	
**Project Dates:Start Date -September 1st 2015 / End Date - November 6th 2015** 
	
**Final Budget: RM20,000**

		
---

1.Did the project meet scope, time, cost and quality goals?
	
	On the start the project, the goal that we set up was working well as planned such as scope, time cost was on course. 
	During the phase 2nd, management decided to change the time frame by a later date of the project because of technical issue and other issue has arrives. 
	This resulted in work schedule shifting and increase workload for project member.


2.What was the success criteria listed in the project scope statement?
	
	The main success criteria listed in our scope statement was if the project take a little longer to complete. 
	The management will still view it as a success if the overall respond on viewer base are positive. 
	To meet the time frame goal, the wiki project must allocate more workload to project member such as developed, tested and after it is rolled out.


3.Reflect on whether or not you met the project success criteria.
	
	About the time part which were unable to fulfilled because of the scope change during the course of phase 2, which it lead into having to shift workload into the later date. 
	But the project has a good feedback from the viewer base which is a sign of success.

	
	
4.In terms of managing the project, what were the main lessons your team learned?
	
	The main lesson we have learned were to have a proper communication with project member. 
	Moving on, the other important thing we learn was to have a proper planning to has to be done before putting everything into work which will lead to saving more time and money. 
	So in the future the scope of the project need not be changed.


5.Describe one example of what went right on this project.
	
	The financial part of the project never over budget which is excellent what lead to the project safely rolled out. 
	There was financially organized between the project members.



6.Describe one example of what went wrong on this project.
	
	When the time to implement the final stage of the project, the project is unable to work on mobile platform. 
	The project was not able to integrate properly which result in lots of setback.



7.What will you do differently on the next project based on your experience working on this project?
	
	The main thing that will be taken care of for the next project will be doing a proper planning before executing the project. 
	If planning was done right from the beginning the result in will be have data of accurate planning of resource, manpower and cost. 
	So the primary focus will be on proper planning so that the next scope of the project will not be changed

	



