## List of Quality Standards for (Educational Wiki for Java Project – If Else)


|No.| Success Criteria/Requirement 									  |	Quality standard				|	Metric					|Quality Assurance	|	Quality Control | Evaluation|
|---| 						 ---  										  |				---					| 			---				|			---		|		---			| 	---		|
|1 	|Viewer are able to view the wiki related to Java If Statement|	5k of viewer have view the wiki within one month of release|	Number of new viewer add into the system per day|	Advertise the wiki on various method examples, (word of mouth, posting on various website, etc.) |	Results: viewer count increase |	Quality standard not met: Need to promote more aggressively |
|	| 						   			---							  |					---				| 				---			|			---		|	Action : To review viewer count				| 	---		|
|2 	|	Viewer are able to view the video related to Java If Statement|	5k of viewer have view the video within one month of release|	Number of new viewer add into the system per day|	Advertise the video on various method examples, (word of mouth, posting on various website, etc.) |	Results: viewer count increase |	Quality standard not met: Need to promote more aggressively|
|	| 						   			---							  |				---					| 		---					|		---			|	Action : To review viewer count				| 	---		|
|3	|Video Content and Quality|	Website link for the video sharing site (YouTube) is working for the video. Able to view in 720pixel, Audio is working with subtitle. |	Video is present on the video sharing site (YouTube) |	Get expert to advice for   the video quality. |	Results: Video is fully functional on YouTube and able to be view without losing Quality |	Quality standard not met: Need to remind the person in charge of video editing to review the video again. |
|	| 						---   										  |				---					| 			---				|		---			|	Action : To review the video quality				| 	---		|
|4 	|Content Friendly|	Viewer comment positive over  are 80% and above|	Amount of positive comment on viewer comment section|	Get a group of  people that don’t understand about Java from different background and ages to give feedback |	Results: Viewer comment overall positive.  |	Quality standard not met: Need to remind the person in charge of video editing to review the video again. |
|	| 						   ---										  |				---					| 			---				|		---			|	Action: Collect feedback and review all comment.				| 	---		|
|5 	|Informative|	Viewers are able to understand on each topic. |	Number of positive comment from the comment section|	Get experts on each topic to write the material|	Results: Viewer comment overall comment positive.  |	Quality standard not met: Need to check in with experts and revise the existing material. |
|	| 						   ---										  |					---				| 			---				|		---			|	Action: Collect feedback and review all comment.			| 	---		|
