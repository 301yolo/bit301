.**Business Case for <Educational Wiki for Java Programming - If Else>**

**September 1st 2015 - November 6th 2015**

**Prepared by: 301Yolo**

| **1.0 Introduction/ Background**  | 

 Team 301Yolo is requested to set up a educational wiki site for a topic in "Java Programming" where the team will be handling on the topic about the "If Else" statement.

The team must include 3 item into the wiki which are:

 1. Information explaining about the Java Programming - If Else 

 2. To include minimum of two image about the Java Programming topic.

 3. A video with a duration minimum of 5 - 10 minute which include both audio and subtitle and to be hosted on a video sharing site.

The information, image and the videos are required to be completely original and accurate without retrieving from other sites.

This project is required to be completed by the 6th of November 2015.

---

| **2.0 Business Objective**     |

| The objective for this business project are:

1. To allow new java students to easily understand on a certain topic in the Java Programming.
2. To enhance java users programming skills.
3. To be able to increase new incoming work for the organization to work on.
4. Provide free Java Programming knowledge for viewers who are curious.
5. Increase the awareness of existence for the organization. 

---

| **3.0 Current Situation and Problem/Opportunity Statement**    | 

 The current situation for this project is that the project is on schedule according the Gantt Chart which was created as the work scheduler, where the first deliverable are able to be delivered. Information explaining about the Java Programming - If Else for the wiki page is currently completed and moving to the stage for reviewer to review about the quality and the understanding for it. 

Problem may appear for team 301Yolo upon handling the quality and understanding of the Java topic if the reviewer or the stakeholder has given bad response towards the wiki page. More time required to be allocated to further improve the quality and understanding for the Java topic and to be reviewed again to receive approval from reviewers and the stakeholders before the due date.

An opportunity may appear when the information for the Java Programming topic is given positive result from its review which allow team 301Yolo to allocate more time on handling the project deliverable such as the image and the video required for the wiki page. The image and video may be made according to the information that was created in the wiki page where viewer may understand easily. The image and video is also required to be review and to attain approval from stakeholder and reviewers. 

---

| **4.0 Critical Assumption and Constraints**   . |


Assumptions:

- The information to create the wiki page is easily understandable.
- The project will be completed before the due date.
- The project will run according to schedule.
- The project will help the organization to gain new work request once successful.
- Stakeholder will be satisfied with the quality of the wiki page once delivered.

Constraints:

- The information that is written in the wiki page must receive approval before the given work time is finish.
- The work may only be completed within the budget given.
- The image and the video must be an original idea and be made within the work time given.
- The wiki page must be able to be view in all platforms.
- The final product must be delivered on November 6th 2015.

---

| **5.0 Analysis of Option and Recommendation**     |


The option's for conducting this projects are: 

1. By develope and design the wiki page base on existing software and hardware.
2. Requesting stakeholder to promote the product once finished. 
3. Purchasing specialized software to help develop the wiki page.
4. By creating prototype version of the wiki page to be evaluated.

The recommendated option to go on this project is option 2 and option 4 to go on with this project.

---

| **6.0 Preliminary Project Requirements**     |

The main features for this project: -Wiki Page If else-


- The wiki page will allow user to view the Java Programming Topic information anytime.
- The wiki page includes image and video which provide additional understanding for the viewers
- The wiki page will be able to be view in all current platforms.
- The wiki page will be host without any restriction access from other locations.
- The project is required to be finished by November 6th 2015.

---

| **7.0 Budget Estimate and Financial Analysis**     |

A estimation of the budget required for the entire project to be completed is RM 20000.00. The estimated budget is base on the development fee of the wiki page.
The following is the estimation of the budget that will be used during the development of the wiki page:

  | Name | Estimated Pricing | 
  |:--------------------------------:|:----------------------:| 
  | Web Page | RM 1000.00 | 
  | Domain Name (per year) | RM 100.00 |
  | Web Hosting (per year) | RM 250.00 |
  | Bulleting | RM 1500.00 |
  | Enquiry Feedback Form | RM 500.00 |
  | Email Capturer | RM 350.00 | 
  | Search Engine Optimization (SEO) | 20%*(3 month) per page |
  | Hosted Email Setup | RM 300.00 | 
  | Logo Design | RM 350.00 | 
  | Purchasing Software & Hardware | RM 5000.00 |
  | Web Maintenance (per month) | RM 120.00 * (3 month) |
  | Data Backup (per month) | RM 200.00 * (3 month) |
  | Hiring Web Designer | RM 2500.00 * (3 month) |

With the Inclusive of GST 6% in Malaysia and with the development time of 3 month for the wiki page, the total project estimated fee is RM 19514.60, which is in range for the estimated budget.

The maintenance fee for the wiki page once is completed are accordingly display in the table below:

  | Name | Estimated Pricing | 
  |:--------------------------------:|:----------------------:| 
  | Domain Name (per year) | RM 100.00 |
  | Web Hosting (per year) | RM 250.00 |
  | Search Engine Optimization (SEO) | 20%*(3 month) per page |
  | Web Maintenance (per month) | RM 120.00 * (12 month) |
  | Data Backup (per month) | RM 200.00 * (12 month) |
  | Hiring Web Designer | RM 2500.00 * (12 month) |
  
As mention above, with the inclusive of GST 6% and per year,the total price for maintenance fee is RM 38,785.40. For year 0, the maintenance fee is RM 28,810.80 due to the remaining 9 months in the year.

With the benefits earn from the videos advertisement view and a thousand per view from it, and the advertising space in wiki page, the following formula will display the total benefit in a year:

While performing analysis with a discount rate of 6%, with the estimated payback is within 4 years, The net present value (NPV) is a total of RM 201,521.45 and the return on investment (ROI) from the analysis is 133%.
  
---

| **8.0 Schedule Estimate**     |


Stakeholder would like this project to be completed within the given due date: 6th November 2015. Base on the schedule, there are time flexibility on creating the wiki page for the stakeholders. The organization wish that this project will help them financially base on the financial analysis.

---

| **9.0 Potential Risks**     |



- The wiki page may not be suitable in all current platforms, where budget may not be sufficient to include them all.
- The information for the wiki page may not receive positive result and may delay the work time for other works.
- The video or the image does not indicate clear explanation where recreating the files required lots of work time.
- Video size or type may not be suitable for video sharing sites to be uploaded.
- The completion of the wiki page may not be able to meet the dateline as there are other projects to be completed.

---

| **10.0 Exhibits** Exhibit A: Financial Analysis (Insert image here) | 

![](https://plus.google.com/117289185199140358281/posts/jLh8Xf55Fxb?pid=6200880160021574882&oid=117289185199140358281)

--- 