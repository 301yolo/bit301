##Status/Progress Report

**Project Name: Educational wiki for Java programming – if else**
 
**Date: 2nd October 2015**

**Reporting Period: 1st September 2015 - 2nd October 2015**

---
 
####Work completed this reporting period:

- Educational Wiki Project:-

	-  Wiki Page Information


- Initiating Tasks :- 
	- Three Sphere Model
	-  Stake Holder Analysis
	-   Business Case
	-   Project Charter


- Planning Tasks :- 

	- Scope Statement
	- Work Breakdown Structure
	- Work Items List
	- Baseline Gantt Chart

- Executing Tasks

- Monitoring and Controlling Task 1 :- 
	- Issue Tracking
	- Tracking Gantt Chart
	- Progress Report


####Work to complete next reporting period:

- Educational Wiki Project :-
	
	- Wiki Page Image

	- Wiki Page Video

- Planning Tasks 2 :- 
	- Cost Estimate
	- Quality Management Plan
	- Risk Management Plan

- Executing Tasks 2

- Monitoring and Controlling Task 2 
	
	- Earned Value Analysis
	- Issue Tracking
	- Tracking Gantt Chart
	- Quality Control
	- Risk Management
	
- Closing Tasks
	- Final Project Presentation
	- Lesson Learned Report




####What’s going well and why:

Base on the gantt chart created, the time for creating the image and video for the wiki page does not required to be rushed as there are no delay in developing the wiki pages information.



####What’s not going well and why:

There is a critical path in the gantt chart for finishing the documentation for this project where it is required to work on the project are: Executing Tasks 2, Monitoring and Controlling Task 2, Planning Task 2 and the Lesson Learned Report.


####Project changes: 

The Gantt Chart has shown that some work has been delayed, which the team requires to take notice on. 


####Strategies for meeting project objectives:

By working on the project base on the timeline given in the gantt chart to meet with the due date for this project to successfully hand in the deliverables.






