##Work Breakdown Structure for Educational wiki for Java programing – if else

Prepared by: 301YOLO				Date: September 25th 2015	

1.0 Educational Wiki Project

	1.1 Wiki Page Information
	
        1.1.1 identify information
		
        1.1.2 consult java expert
		
        1.1.3 create content

    1.2 Wiki Page Image
	
        1.2.1 research image creator software
		
        1.2.2 purchase image creator software
		
        1.2.2 develop image

    1.3 Wiki Page Video
	
        1.3.1 research video creator software
		
        1.3.2 research video creator software
		
        1.3.3 develop video
		
        1.3.4 upload video to video hosting sites
	

2.0 Initiating Tasks

	2.1 Three Sphere Model
	
	2.2 Stake Holder Analysis
	
	2.3 Business Case
	
	2.4	Project Charter

3.0 Planning Tasks

	3.1 Scope Statement
	
	3.2 Work Breakdown Structure

	3.3 Work Items List

	3.4 Baseline Gantt Chart

4.0 Planning Tasks 2

	4.1 Cost Estimate

	4.2 Quality Management Plan

	4.3 Risk Management Plan

5.0 Executing Tasks

6.0 Executing Tasks 2

7.0 Monitoring and Controlling Task 1

	7.1 Issue Tracking
	
	7.2 Tracking Gantt Chart

	7.3 Progress Report

8.0 Monitoring and Controlling Task 2

	8.1 Earned Value Analysis as of 15 Oct 2015

	8.2 Issue Tracking
	
	8.3 Tracking Gantt Chart

	8.4 Quality Control

	8.5 Risk Management

9.0 Closing Tasks

	9.1 Final Project Presentation

	9.2 Lesson Learned Report

	9.3 User Acceptance Signed
		
		