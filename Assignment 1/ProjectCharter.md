## Project Charter

**Project Title** : Educational wiki for Java programming – if else

**Project Start Date:** September 1th 2015

**Projected Finish Date:** November 6th 2015

**Budget Information:** 
Estimated RM 20,000++ for the budget to develope the wiki page.

**Project Manager:** 

Name: Tan Khoon Hoong                
E-mail: damentan101@gmail.com              


**Project Objectives:**

To provide a beginner’s introduction to the java platform about if else statement. 


**Main Project Success Criteria:**

Manage to deliver programming information for the beginner
Manage to provide a fully functional wiki page



**Approach:**

-	Discuss with our member as much as possible for planning and analysis
-	Hire a Web Designer to help develop the wiki page
-	Discuss and analysis the wiki page with Java Professional.
-   Provide a detailed cost estimation for the wiki page.

**Roles and Responsibilities**


| Role | Name | Organization | Position | Email |
|------|------|--------------|----------|-------|
| Designer        | Hay Lik Sheng       | 301YOLO  | Developer         | harrisonliksheng@gmail.com       |
| Designer        | Chong Kam Fai       | 301YOLO  | Developer         | asharox2@gmail.com               |
| Team Leader     | Tan Khoon Hoong     | 301YOLO  | Project Manager   | damentan101@gmail.com            |
|      |      |              |          |       |

**Sign-off:** (Signatures of all above stakeholders. Can sign by their names in table above.)
Sign off by Tan Khoon Hoong

**Comments:** (Handwritten or typed comments from above stakeholders, if applicable)
No Comment