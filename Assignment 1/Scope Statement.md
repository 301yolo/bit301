##Scope Statement (Version 1.0)

###Project Title: Educational Wiki for Java Programing – If Else
####Date:	September 1st 2015 - November 6th 2015	
####Prepared by: 301yolo

---

**Project Justification:**

This project will establish a wiki site as a "Educational Wiki for Java Programming - If-Else Statement" for user that has a passion to learn and further understand Java Programming. It will consist of one wikipage to support the needs and will find potential resource by acquiring related work items and resources as well as engaging with Java Programming Expert to identify area that needed to be improve.
The Project will:

	1. Develop a wiki page.
		
	2. Set up procedures and policies for the use of the wiki.

	3. Develop and distribute strategic to the work group, and
	
	4. Clarify ongoing support and maintaince requirements.


---

**Product Characteristics and Requirements:**

This Product Characteristics of the project is to establish a wiki site as a "Educational Wiki for Java Programming - If-Else Statement" which user be able to access and study on it.The wikipage will consist of a video tutorial and image explanation to provide a further understanding on If-Else Statement. User will be able to access the wikipage anywhere via internet and across multiple platforms.
The following requirements haven been identified for the Wikipage Project:
	
	1. Accessible from anywhere via internet.
	
	2. Accessible from All platform such as desktop computer/mobile.
	
	3. Ability to have tutorial video.

	4. Ability to host a social platform with access granted to all visited user.
---

**Summary of Project Deliverables**

The product that will be delivered through the end of the project is to create a java wiki page to allow user to understand about it by showing

1. Develop a wiki page.

	1.1 Set up image and video.
		
2. Set up procedures and policies for the use of the wiki.

3. Develop and distribute strategic to the work group, and
	
4. Continued distribute, enchancement and provide support for the wikipage 

5. Close Project and update all related material.

---
	
Project Success Criteria: 

Criteria have been established for the wikipage project to ensure thorough setting and succesful completion of the project. 

All Criteria must be met to achieve success for this project :

	1. Meet all deliverables within schedule time and budget tolerances.
	
	2. Manage to deliver programming information for the user to learn the basic of Java If-Else Statement.  
	
	3. Manage to provide a fully functional wiki page and user friendly easy to search what people needs.  
	
	4. Manage to allow user to view the Java Programming TOpic information at anytime provided that internet access is available.  
	
	5. Manage to view across all current avaible platform such as desktop computer platforms and mobile platforms.
	
	
---



				
				
				
				
				
				
				
				
				
				
				
				

