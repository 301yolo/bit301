#Three Sphere Model Analysis for Educational wiki for Java programing – if else#

**Team Members:**
Tan Khoon Hoong,
Hay Lik Sheng,
Chong Kam Fai

###Introduction###
Team 301Yolo is requested to set up a educational wiki site for a topic in "Java Programming" where the team will be handling on the topic about the "If Else" statement.

The team must include 3 item into the wiki which are:

 1. Information explaining about the Java Programming - If Else 

 2. To include minimum of two image about the Java Programming topic.

 3. A video with a duration minimum of 5 - 10 minute which include both audio and subtitle and to be hosted on a video sharing site.


###Stakeholder Analysis###

*Each team member must identify one stakeholder each and discuss their expectations and how you will manage them*

Project Sponsor - Sponsor funding for the wiki page. Give them daily update about the project on going.

Project Team - Monitoring each team member to work on the project. Give them a clearly explanation for the wiki page and how to do it.

Image Editor Expert - Help us to design the image and put it on wiki page. Give them a clearly explanation for what image should edit and put it on wiki page.

Java Programming Expert - Help us in the coding and develop the wiki page. Ask for some coding advise about coding to develop the wiki page.

Video Editor Expert - Help us to edit the video and put in on wiki page. Give them them video we recoded and helpingus to edit it and put in on wiki page

###Business Issues###



1. What will the amount of cost spend on the project?
2. What will it cost student?
3. Will the wiki page able to challenges of globalazation as their top concern?
4. Will the term of uncertainly effect the wiki page?


###Technology Issues###

1. Should the wiki page use Joomla, Coffee Cup, HTML5(Website design software) to design?
2. Should this wiki page work on all multiple platforms such as tablet or mobile devices?
3. Will the wiki page take a long time?
4. Will the wiki page security good? Easy to be hack? 


###Organization Issues###

1. who will be that one to administrate an suppport the wiki page?
2. Will the project affect all people or just certain targeted majors?
3. Will the organization provide a clear growth path for the employee?
4. Will the manager communicate and monitor the employee regulary?


####Conclusion
Find more Sponsorship and make a better idea then other company. Get more simple tools that can develop the wiki page. Our organization should hire more staff to develop our wiki page.  


 
